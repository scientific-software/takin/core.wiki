[Back to Contents](Home).

## Frequently Asked Questions
- Why does the resolution convolution [simulation](Resolution-Convolution) or [fit](Convolution-Fitting) not start?  
  This might be due to a configuration error or -- in the case of [*Python* S(Q, E) models](Python-S(Q,-E)-Models) -- a syntax error in the *Python* script file. As the convolution tool delegates the actual work to sub-processes, it does not directly show errors from these processes. Instead any errors can be found in the log file (in *Takin's* main window select "Help" -> "Log..." from the menu) or on the console if *Takin* is launched from the command line.
- Will fitting/data treatment feature X be added to the *Scan Viewer*?  
  The *Scan Viewer* is a lightweight tool used to have a quick look at scan data files while featuring some very basic fitting. Adding advanced features would make it more difficult to use for the simple tasks it was designed for. Advanced data treatment will instead be available in the separate *Scan Browser* of which a preview version is already available in *Takin*.