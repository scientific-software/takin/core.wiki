[Back to Contents](Home).

## Set-up
For calculating the instrumental resolution at a given Q = (hkl) and E position the instrumental parameters have to be set-up first. This can be done using the option "Resolution" -> "Parameters..." in the menu bar.

In the "Resolution Parameters" dialog box, an algorithm has to be set first. This is done in the tab "Calculation". Depending on the selected algorithm various settings tabs will be enabled. These contain -- among other things -- the geometrical description of the instrument.

Once all parameters are entered, a valid resolution matrix should be displayed in the "Calculation" tab.

Instrument configurations can either be saved using the "Save..." button in the "Resolution Parameters" dialog or via the menu bar in the *Takin* main window. The latter option saves the resolution settings together with the crystal definition.

## Resolution Visualisation
Clicking on "Resolution" -> "Ellipses..." in the menu bar shows a live view of the resolution function at the current instrument position. The blue (green) ellipse is a sliced (projected) view of the four-dimensional ellipsoid using the given projection planes.

Visualisation of the resolution ellipses can be done in several coordinate systems that are selected using the combo box at the bottom of the dialog: The first option shows the momentum transfers in inverse Angstroms in the standard coordinate system with x along Q and y perpendicular to Q. The second option chooses the crystal's rlu coordinate system with the h axis along x, k along y and l along z. The third option also uses the crystal's rlu system, but with the x and y axis along the Bragg reflexes chosen on the "Recip. Plane" tab in the *Takin* main window.

## Background information
The resolution function of a triple-axis spectrometer is usually modelled employing Gaussian transmission functions for the neutron-optical components (monochromator/analyser crystals, collimators, etc.), resulting in a four-dimensional Gaussian distribution function for the instruments momentum transfer vector Q and energy transfer E.

The four-dimensional half-width at half-maximum (HWHM) hyper-surface of the resolution function, as described by the resolution matrix, is a quadric, namely an ellipsoid in four dimensions (three dimensions for the Q vector and one for E). The ellipsoid's principal axis radii and angles can be found via the eigenvalues and -vectors of the resolution matrix.

More details can be found [here](http://dx.doi.org/10.1107/S0567739475001088) or [here](http://dx.doi.org/10.1016/j.nima.2014.03.019).