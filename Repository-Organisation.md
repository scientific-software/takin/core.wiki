[Back to Contents](Home).

The source code [repository group](https://code.ill.fr/scientific-software/takin) is organised as follows:

- The [core](https://code.ill.fr/scientific-software/takin/core) repository contains the central *Takin* modules, which are licensed under the [GNU GPL version 2](https://code.ill.fr/scientific-software/takin/core/-/raw/master/COPYING). They are written in C++14. These modules comprise, for example,
  - the [real and reciprocal space visualisation](https://code.ill.fr/scientific-software/takin/core/-/tree/master/tools/taz),
  - the [resolution calculator](https://code.ill.fr/scientific-software/takin/core/-/tree/master/tools/res), [convolution simulator](https://code.ill.fr/scientific-software/takin/core/-/tree/master/tools/monteconvo) and [fiitter](https://code.ill.fr/scientific-software/takin/core/-/tree/master/tools/convofit),
  - the [scan viewer](https://code.ill.fr/scientific-software/takin/core/-/tree/master/tools/scanviewer),
  - etc.

- The [mag-core](https://code.ill.fr/scientific-software/takin/mag-core) repository contains the *Takin* modules having to do with (mostly) magnetic and polarised scattering. They are licensed under the [GNU GPL version 3](https://code.ill.fr/scientific-software/takin/mag-core/-/raw/master/LICENSE). Some other, non-magnetic modules also reside here because they use the same license and codebase as the magnetic ones. They are written in C++20. The modules in this group comprise, for example,
  - the [magnon calculator](https://code.ill.fr/scientific-software/takin/mag-core/-/tree/master/tools/magdyn),
  - the [magnetic](https://code.ill.fr/scientific-software/takin/mag-core/-/tree/master/tools/magstructfact) and [nuclear](https://code.ill.fr/scientific-software/takin/mag-core/-/tree/master/tools/structfact) structure factor calculators,
  - the [polarisation vector calculator](https://code.ill.fr/scientific-software/takin/mag-core/-/tree/master/tools/pol),
  - the [CIF file loader](https://code.ill.fr/scientific-software/takin/mag-core/-/tree/master/tools/cif2xml),
  - the [Brillouin zone calculator](https://code.ill.fr/scientific-software/takin/mag-core/-/tree/master/tools/bz),
  - the [scan browser](https://code.ill.fr/scientific-software/takin/mag-core/-/tree/master/tools/scanbrowser) for treating polarised scan data,
  - etc.

- The [tlibs](https://code.ill.fr/scientific-software/takin/tlibs) repository contains the physical-mathematical template library *tlibs* version 1 that is used by the core module. *Tlibs* is licensed under either the [GNU GPL version 2](https://code.ill.fr/scientific-software/takin/tlibs/-/raw/master/COPYING.GPLv2) or the [GNU GPL version 3](https://code.ill.fr/scientific-software/takin/tlibs/-/raw/master/COPYING.GPLv3). Its code is written in C++11 / C++14.

- The [tlibs2](https://code.ill.fr/scientific-software/takin/tlibs2) repository contains the physical-mathematical template library *tlibs* version 2 that is used by the mag-core module. *Tlibs 2* is licensed under the [GNU GPL version 3](https://code.ill.fr/scientific-software/takin/tlibs2/-/raw/master/LICENSE). Its code is written in C++20.

- The [data](https://code.ill.fr/scientific-software/takin/data) repository contains example data files and tutorials for *Takin*.

- The [setup](https://code.ill.fr/scientific-software/takin/setup) repository contains scripts to build and install *Takin*.

- The [magnons](https://code.ill.fr/scientific-software/takin/plugins/magnons) repository contains the magnetic dynamics plugin module for the [resolution convolution simulator/fitter](Resolution-Convolution). It is described [here](Magnon-S(Q,-E)-Model).

- The [mnsi](https://code.ill.fr/scientific-software/takin/plugins/mnsi) repository contains the helimagnon and skyrmion dynamics plugin module for the [resolution convolution simulator](Resolution-Convolution) that was used in [our skyrmion paper](http://dx.doi.org/10.1126/science.abe4441).

- The [paths](https://code.ill.fr/scientific-software/takin/paths) repository contains the triple-axis pathfinder, which is described in more detail [here](https://github.com/ILLGrenoble/taspaths/wiki).