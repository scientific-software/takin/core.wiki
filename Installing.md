[Back to Contents](Home).

## On *Ubuntu*
- Download the package for your version of *Ubuntu* [here](https://wiki.mlz-garching.de/takin).
- Install the downloaded package using: `sudo apt install --fix-broken ./<takin.deb>`, where "<takin.deb>" is the name of the downloaded package. Please note the "./" before the file name.
- [ Alternatively, you can install the package using the commands `sudo dpkg -i <takin.deb>` followed by `sudo apt install --fix-broken`. ]

## On *Mac*
- Download the dmg image for *Mac* [here](https://wiki.mlz-garching.de/takin).
- Open the dmg file and drag the *Takin* icon onto the icon to the left representing the /Applications folder.
- Once installed, launch *Takin* from the /Applications folder. Note that the first time you need to control-click on *Takin* and select "Open". This is because the program is not signed.
- [ An older, signed, but feature-reduced version of *Takin 2.0* is also available from the *App Store*. Due to its missing functionalities involving resolution convolution, this version is not recommended for data treatment. ]