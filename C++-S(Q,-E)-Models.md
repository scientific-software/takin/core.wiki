[Back to Contents](Home).

*Takin* can load S(Q, E) plugins via native shared libraries (SO or DLL files). C++ examples to build upon are given in the subdirectory "examples/sqw_module".

Plugin binaries have to be placed in the directory "~/.takin/plugins" to be loaded.

A fully worked-out example to build upon can be found [here](https://code.ill.fr/scientific-software/takin/core/-/tree/master/examples/sqw_module). The new magnon plugin module can be found [here](https://code.ill.fr/scientific-software/takin/plugins/magnons).