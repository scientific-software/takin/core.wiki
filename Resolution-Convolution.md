[Back to Contents](Home).

In order to plan an experiment or to get an overview before using the more involved convolution fitter, a live Monte-Carlo convolution can be performed by selecting "Resolution" -> "Convolution..." in the menu bar.

In the "Convolution" dialog four things have to be defined: (1) a crystal, (2) an instrument, (3) a theoretical model for the dynamical structure factor S(q,E), and (4) a scan direction:

(1) and (2): The crystal (together with a scattering plane) and the instrument parameter files are entered in the "Crystal File" and "Resolution File" edit fields, respectively. These files are the ones that are defined [in the Takin main window](Basic-Usage) and [in the "Resolution Parameters" dialog](Resolution-Calculation). Instead of manually defining a crystal and a scan direction, the relevant parameters can be automatically determined from a TAS data file, which can be optionally entered in the "Measurement" edit field. Multiple scan files can be specified by separating them with a semicolon (;).

(3): The S(Q, E) model is a user-provided theoretical formula or algorithm for the physical system to be measured. It can be either provided as a [Python](Python-S(Q,-E)-Models) or a [Julia](Julia-S(Q,-E)-Models) script, or as a [native plugin](C++-S(Q,-E)-Models). Several simple S(Q, E) models are included internally for testing. Each model provides variables which can be directly changed in the GUI via the "Parameters..." button next to "Model Source". These variables are the same that are also exposed to the fitter.

(4): The actual scan path is defined in the "Scan Steps" group at the bottom of the dialog. Here, the Q=(hkl) and E coordinates of the start end end position are entered along with the number of subdivisions between these positions ("Steps").

## Background information
A triple-axis spectrometer does not sample the dynamical structure factor S(q, E) at the nominally set-up reciprocal coordinates (q, E), but instead probes S in a normally-distributed range ([q - σ_q, q + σ_q], [E - σ_E, E + σ_E]), where the deviations σ_q and σ_E are given by the resolution ellipsoid. Mathematically, the measured intensity is the convolution integral of the dynamical structure factor and the instrumental resolution function. In *Takin*, the integral is solved numerically using Monte-Carlo integration.

More details can be found [here](http://dx.doi.org/10.1107/S0567739475001088) or [here](http://dx.doi.org/10.1016/j.nima.2014.03.019).