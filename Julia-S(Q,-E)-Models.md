[Back to Contents](Home).

Similarly to the [Python modules](Python-S(Q,-E)-Models), a S(Q, E) [Julia](https://julialang.org) module has to define the two functions "TakinInit" and "TakinSqw".

"TakinInit" is called after one or several parameters have changed (for example after each minimisation step in the convolution fitter). It can be used to check if e.g. pre-calculated tables, variables, etc. need to be recalculated.

The "TakinSqw" function receives four floating-point parameters h,k,l, and E and returns a single floating-point value S, the dynamical structure factor. The function is called for every Monte-Carlo point.

All global variables that are defined in an S(Q, E) Julia module and that are prefixed with "g_" (for "global") are made available as settable parameters in the convolution dialog and as fit parameters for the convolution fitter.

The minimal interface to *Takin* is defined as follows:

```
function TakinInit()
	# reinitialise variables here
end

function TakinSqw(h::Float64, k::Float64, l::Float64, E::Float64)::Float64
	S = 0.
	# calculate S here
	return Float64(S)
end
```

A fully worked-out example to build upon can be found [here](https://code.ill.fr/scientific-software/takin/core/-/tree/master/examples/sqw_jl).
