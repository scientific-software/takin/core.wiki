[Back to Contents](Home).

## Setup
All demos for using the resolution convolution can be found on-line [here](https://code.ill.fr/scientific-software/takin/data/-/tree/master/demos/phonon) or [here](https://github.com/ILLGrenoble/takin/tree/master/data/demos/phonon) (in case the main repository is off-line). To use the demos, please download the data archive from [here](https://code.ill.fr/scientific-software/takin/data/-/archive/master/data-master.zip) or [here](https://codeload.github.com/tweber-ill/ill_mirror-takin2-data/zip/refs/heads/master) and unpack it.

## Running the Phonon Simulation

(1) Open the [convolution dialog](Resolution-Convolution) using "Resolution" -> "Convolution..." in *Takin's* main menu.

(2) Select "File" -> "Open..." in the convolution dialog's menu and navigate to the folder where you downloaded and extracted the data archive (see above). Go to the sub-folder "data/demos/phonon" and open the file "phonon_simple_py.taz".

(3) Click the "Start Sim." button to calculate a resolution-convolution simulation.

The demo convolution uses the [Python backend](Python-S(Q,-E)-Models) for creating S(Q, E) models. In the example, the model file can be found in the file "sqw_phonon.py". In there, the most important (and only mandatory) interface function for *Takin* is "TakinSqw(h, k, l, E)", which receives the (hkl) coordinates (in rlu) and the energy transfer E (in meV) of each random Monte-Carlo neutron events from *Takin*, and has to return the dynamical structure factor S(h, k, l, E) for the queried coordinate. How the structure factor is calculated is up to the user. In the example, a simple sinusoidal phonon dispersion branch with a DHO shape in energy is created.


## Fitting the Phonon Data

Each parameter in the Python S(Q, E) model script (here: "sqw_phonon.py") which is global and whose name begins with "g_" is a parameter that can be changed directly from *Takin* and that can also be defined as a fit parameter. In the convolution dialog, they can be inspected and modified by clicking on the "Parameters..." button.

(4) Fitting a parameter is done by checking the respective "Fit" column in the parameters dialog and giving the parameter a non-zero error. Initial values for fitting can be set via the "Value" column.

(5) If at least one parameter has been declared a fit parameter, the convolution fitting can be started by clicking "Start Fit".

(6) If the curve appears to be completely at zero in the plot window, try increasing the "Scale" factor, for example to "1e4" or "1e5". 