Welcome to the *Takin* documentation!

<img src="https://code.ill.fr/scientific-software/takin/data/-/raw/master/res/icons/takin.svg?inline=false" width="10%" height="10%" title="Logo" alt="">

*Takin* is a software package for inelastic neutron scattering which features a full GUI, but can also be used as a library. It can be employed for [lattice and scattering plane visualisation](Basic-Usage), triple-axis and time-of-flight spectrometer [resolution calculation](Resolution-Calculation), planning of measurements using a live [convolution preview](Resolution-Convolution), and [convolution fitting](Convolution-Fitting).

**Note that this is an internal development version of Takin and its documentation. Please go [here](https://github.com/ILLGrenoble/takin/wiki) for the main version.**

## Contents
1. Setup
   - [Installing Takin](Installing)
   - [Compiling Takin](Compiling)
   - [Repository Organisation](Repository-Organisation)
2. [Basic Usage](Basic-Usage)
3. [Resolution Calculation](Resolution-Calculation)
4. [Resolution Convolution Simulation](Resolution-Convolution)
5. [Resolution Convolution Fitting](Convolution-Fitting)
6. Convolution Modules
   - [Magnon S(Q, E) Model](Magnon-S(Q,-E)-Model)
   - [Internal S(Q, E) Models](Internal-S(Q,-E)-Models)
7. Convolution Scripting Modules
   - [Python S(Q, E) Models](Python-S(Q,-E)-Models)
   - [Julia S(Q, E) Models](Julia-S(Q,-E)-Models)
   - [C++ S(Q, E) Models](C++-S(Q,-E)-Models)
8. Tutorials
   - [Magnon Convolution](Magnon-Convolution-Tutorial)
   - [Phonon Convolution](Phonon-Convolution-Tutorial)
9. [Frequently Asked Questions](Frequently-Asked-Questions)
10. Links
    - [Code Repository](https://github.com/ILLGrenoble/takin)
    - [Development Repositories](https://code.ill.fr/scientific-software/takin)
    - [Takin's Website](https://wiki.mlz-garching.de/takin)
