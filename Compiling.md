[Back to Contents](Home).

## Compiling Takin
- Either clone *Takin's* development repository and its submodule dependencies using the following command:  
```bash -c "`curl https://code.ill.fr/scientific-software/takin/setup/-/raw/master/checkout.sh`"```,  
  or get the release source code repository:  
```git clone https://github.com/ILLGrenoble/takin```.
- Go to the "setup" directory: `cd takin/setup`. 
- Locate and run the build script for your system, e.g. `./build_lin/build_jammy.sh`.


## Compiling the Magnetic Dynamics Plug-In Module
- After having cloned the source repository (see first point above in "Compiling Takin"), go to the plugin's directory: `cd takin/plugins/magnons`.
- Create a "build" directory: `mkdir build && cd build`.
- Build the plugin: `cmake -DCMAKE_BUILD_TYPE=Release .. && make -j4`.
- Copy the plugin to *Takin's* plugin directory:
  - On Mac: `cp libmagnonmod.dylib ~/.takin/plugins/`,
  - On Linux: `cp libmagnonmod.so ~/.takin/plugins/`.