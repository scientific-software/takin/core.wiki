[Back to Contents](Home).

The main window is divided into two parts: The left-hand side is used for reciprocal-space visualisations: The tab "Reciprocal Lattice" shows the Bragg peaks in the current scattering plane and the scattering triangle, the tab "Projection" shows various projective views, e.g. for calculating Laue patterns. The right-hand side of the window depicts several real-space visualisations: Here, the real lattice and the crystal's unit cell are shown (tab "Real Lattice") as well as the current instrumental position for triple-axis spectrometers (tab "TAS Instrument") and for time-of-flight spectrometers (tab "TOF Instrument").

The lattice constants and angles of the crystal's unit cell are entered in the lower-left part of the *Takin* main window using either the "Real Lattice" or the "Recip. Lattice" tab. The tab "Recip. Plane" defines the scattering plane used by the spectrometer.

If a space group is defined (lower-central part of the main window), the forbidden reflections are excluded in the reciprocal-lattice view. A cut of the crystals first Brillouin zone is displayed in the reciprocal-space view. The Brillouin zone can also be visualised in three dimensions using the "Reciprocal Space" -> "3D Brillouin Zone..." menu item.

An extended version of the Brillouin zone calculator is available by selecting "Tools" -> "Brillouin Zones..." from the menu. This dialog enables advanced calculations, e.g. arbitrary cuts of the Brillouin zone.

Furthermore, the structure factor and the crystal's unit cell are displayed if both the space group and the atom positions are known: they can be entered using the "Atoms..." button at the bottom of the main window.
