[Back to Contents](Home).


## Setup

All demos for using the resolution convolution with the magnon calculator can be found on-line [here](https://code.ill.fr/scientific-software/takin/data/-/tree/master/demos/magnon) or [here](https://github.com/ILLGrenoble/takin/tree/master/data/demos/magnon) (in case the main repository is off-line). To use the demos, please download the data archive from [here](https://code.ill.fr/scientific-software/takin/data/-/archive/master/data-master.zip) or [here](https://codeload.github.com/tweber-ill/ill_mirror-takin2-data/zip/refs/heads/master) and unpack it. 

## Running the Magnon Simulation

Please follow these steps to run the demo magnon resolution convolution simulation in the graphical user interface.

(1) Open the [convolution dialog](Resolution-Convolution) using "Resolution" -> "Convolution..." in *Takin's* main menu.

(2) Select "File" -> "Open..." in the convolution dialog's menu and navigate to the folder where you downloaded and extracted the data archive (see above). Go to the sub-folder "data/demos/magnon" and open the file "magnon_convolution.taz".

(2a) In case the plugin providing the [magnon model](Magnon-S(Q,-E)-Model) is not installed (i.e, when "Magnetic Dynamics" is not found as a possible model source), it can be built using the instructions [here](Compiling). After building and installing the module, *Takin* and these tutorial steps have to be restarted.

(3) Click the "Start Sim." button to calculate a resolution-convolution simulation.

(4) You can visualise and change the magnetic model by loading it into the magnon caclulation module. To do so, select "Tools" -> "Magnetic Dynamics" in *Takin's* main window.

(5) In the magnon dialog, select "File" -> "Open..." from the menu and select the file "antiferromagnetic_chain.magdyn" from the sub-folder "data/demos/magnon" of the extracted data archive. You can edit and save the model, and the results will be immediately visible in the convolution dialog when doing a re-simulation.


## Simulating via Command Line

For advanced use cases, the magnon convolution simulation can also be run via the command line using these commands:
```
cd data/demos/magnon
takin.sh --convosim magnon_convolution.taz
```
The output of the simulation is written to the file "out.dat", which contains the (hkl) and E coordinates of the simulated neutrons together with the convoluted dynamical structure factor S(Q, E) in a column-based format. The results can, for example, be plotted using: 
```
gnuplot -p -e "plot \"out.dat\" u 1:5 w points pt 7"
```


## Details

The following files are used in the simulation:
- "magnon_convolution.taz": This is the main file controlling the simulation job. Files of this type are created using the [convolution dialog](Resolution-Convolution), which can be found under "Resolution" -> "Convolution..." from *Takin's* main menu. In the convolution dialog (not *Takin's* main window), select "File" -> "Save As..." to write such a file.
- "antiferromagnetic_chain.magdyn": This file describes the magnetic system and is used by the [magnon calculation module](Magnon-S(Q,-E)-Model). Files of this type are created using the magnetic dynamics tool, which can be found under "Tools" -> "Magnetic Dynamics..." in *Takin's* main menu.
- "sample.taz": The information about the sample's lattice and scattering plane is contained in this file. Files of this type are created from *Takin's* main window by selecting "File" -> "Save As...".
- "instr.taz": The instrument is described in this file. Files of this type are created using the [resolution calculation dialog](Resolution-Calculation), which can be opened from the menu of *Takin's* main window by selecting "Resolution" -> "Parameters...".

All input files are in the XML format. This way, they can be edited by external tools or scripts for automatic simulations.