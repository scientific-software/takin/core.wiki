[Back to Contents](Home).

The convolution fitter finds the best least-squares fit of one or several free parameters in an S(Q, E) model using a Monte-Carlo convolution approach. The S(Q, E) model is fitted to one or more given measurement data files.

The convolution fitter can be used either directly form the GUI or alternatively from the command line.

## GUI Version

If a simulation has already been set up [in the "Convolution" dialog](Resolution-Convolution), a resolution convolution fit can simply be performed by designating S(Q, E) model variables as open fit parameters:

(1) Click on the "Parameters..." button next to the "Model Source" selection and tick the "Fit" checkbox for each parameter you want to use in the fit.

(2) The value of the parameter will be used as initial guess, additionally a non-zero error has to be assigned to each fit parameter to serve as fitting uncertainty. Optionally, the range of the fitting region can be limited in the "Limits" column.

(3) Once the parameters are set up, click on "Start Fit".

## Command Line Version

The fitter is called on the command line by: "takin --convofit my_fit.job". The input job file "my_fit.job" in this example has the following structure:

Calling "convofit" without command-line arguments shows its global options.

```
; convofit sample job file

; input files
input
{
    ; file with the TAS scan
    ; multiple files can be entered with a ';' separator
    scan_file       "./my_measurement.dat"

    ; fitting to the data of multiple scan files can be done by defining
    ;scan_file_0     "./my_measurement_0.dat"
    ;scan_file_1     "./my_measurement_1.dat"
    ; ...

    ; temperature column in the scan file
    temp_col        "Ts"

    ; field column in the scan file
    field_col        "Bs"

    ; counter column in the scan file
    counts_col      "ctr1"

    ; monitor counter column in the scan file
    monitor_col     "mon2"

    ; normalise detector counts to monitor counts
    norm_to_monitor 1

    ; manually set scan variable (0=auto, 1=h, 2=k, 3=l, 4=E)
    ;scan_axis      0

    ; for multipe data files, individual axes can be selected using
    ;scan_axis_0    0
    ;scan_axis_1    0
    ; ...

    ; overrides
    ;temp_override   "25"
    ;field_override  "0.5"


    ; the instrument definition created by
    ; the Takin resolution parameter dialog
    instrument_file "./my_instrument.taz"


    ; which S(q,E) model to use?
    sqw_model       "py"

    ; S(q,E) input file. Here, a Python script
    sqw_file        "./my_sqw.py"

    ; how is the temperature/field variable named
    ; the S(q,E) model? Here, "g_T" and "g_H" are
    ; global variables in the "my_sqw.py" script.
    sqw_temp_var    "g_T"
    sqw_field_var   "g_H"

    ; fix some variables in the S(q,E) model
    sqw_set_params  "g_my_param = 12.3"

    ; flip the sense of the coordinate system
    flip_lhs_rhs    0
}


; output files
output
{
    ; a simplified copy of the original scan file for easier reading
    ; in an external plot program
    scan_file        "./my_scan.dat"

    ; a file describing the fit results
    model_file       "./my_model.dat"

    ; logs
    log_file         "./my_log.dat"


    ; show a plot at the end of the fit
    plot              1

    ; show live plots during fitting (very useful for debugging)
    ; gnuplot has to be installed for this to work
    ; (see "Set-up" section of this manual)
    plot_intermediate 1

    ; specify an available gnuplot terminal
    ; list all available terminals using: gnuplot -e "set term"
    ; typically use: "x11", "qt", or "wxt"
    ; if no plot window appears, this is set to the wrong value!
    plot_term         "x11 noraise"
}


; includes a fit settings file (see below)
#include "my_settings.job"
```


The fitting steps are described in the include file "my_settings.job" which is given here: 

```
; convofit sample fit settings file

; Monte-Carlo settings
montecarlo
{
	; number of Monte-Carlo neutrons
	neutrons    10000

	; perform a secondary convolution of the sample shape (still beta)
	sample_positions    1

	; keep the same random seed for more stability
	recycle_neutrons    1
}


; Resolution algo settings
resolution
{
    ; which algorithm to use?
    algorithm    "eck"    ; "cn", "pop", "eck", or "vio"

    ; include the "resolution volume" prefactor?
    use_r0       1

    ; use optimum vertical/horizontal monochromator/analyser focusing?
    focus_mono_v 1
    focus_mono_h 0
    focus_ana_v  0
    focus_ana_h  1
}


; Fitter settings
fitter
{
    ; do a convolution fit or just a plain convolution
    ; using the initial values?
    do_fit        1

    ; which minimiser to use?
    minimiser    "simplex"    ; "simplex" or "migrad"

    ; which Minuit strategy?
    strategy      1         ; 0 (low), 1 (medium) or 2 (high)

    ; number of maximum function calls
    max_funccalls 500

    ; Minuit's targeted "estimated distance to minimum"
    tolerance     10.

    sigma         1.
}


; which S(q,E) model parameters should be fitted?
; don't remove "scale", "slope" or "offs"!
fit_parameters
{
    ; the fit parameters: "scale", "slope" and "offs" are internal variables,
    ; "g_linewidth" is a parameter in the S(Q, E) model. In this example,
    ; it is a global name in the "my_sqw.py" script.
    params  "scale " \
            "slope " \
            "offs " \
            "g_linewidth "

    ; initial values of the three parameters
    values  "1e9 " \
            "0 " \
            "1e-6 " \
            "0.02 "

    ; errors of the three parameters
    errors  "0.5e9 " \
            "0 " \
            "0.5e-6 " \
            "0.01 "

    ; which parameters should be fitted?
    ; here, the fourth parameter, i.e. g_linewidth, is the only
    ; fit parameter
    fixed   "1 1 1 0 "
}
```